<?php

/*
 * Saves an individual card. If $card->cid is specified, the card is updated.
 * If $card->cid is unspecified, the card is created.
 */
function dex_card_save($card) {
  if ($card->cid) {
    if (drupal_write_record('dex_card', $card, 'cid')) {
      return TRUE;
    }
    return FALSE;
  }
  else {
    if (drupal_write_record('dex_card', $card)) {
      return db_last_insert_id('dex_card', 'cid');
    }
    return FALSE;
  }
}

/*
 * Load an individual card.
 */
function dex_card_load($cid, $refresh_cache = FALSE) {
  static $cache = array();
  
  // Load into cache if not already present
  if (!array_key_exists($cid, $cache) || $refresh_cache) {
    $cache[$cid] = db_fetch_object(db_query('SELECT * FROM {dex_card} WHERE cid = %d', $cid));
  }
  
  return $cache[$cid];
}

/*
 * Check if for object $type with ID $id there is a map available.
 */
function dex_has_map($id, $type) {
  $res = db_query('SELECT * FROM {dex_card} WHERE ' . substr($type, 0, 1) . 'id = %d', $id);
  while($card = db_fetch_object($res)) {
    if ($card->longitude != 0 || $card->latitude != 0) {
      return TRUE;
    }
  }
  return FALSE;
}

/*
 * Load all cards for an object.
 */
function dex_card_list($id, $type) {
  $res = db_query('SELECT * FROM {dex_card} WHERE ' . substr($type, 0, 1) . 'id = %d ORDER BY weight, title', $id, $type);
  while ($row = db_fetch_object($res)) {
    $options[] = $row;
  }
  return $options;
}

/*
 * Makes $card the primary 
 */
function dex_card_primary($card) {
  if ($card->nid) {
    db_query('UPDATE {dex_card} SET weight = 0 WHERE nid = %d', $card->nid);
  }
  else if ($card->uid) {
    db_query('UPDATE {dex_card} SET weight = 0 WHERE uid = %d', $card->uid);
  }
  db_query('UPDATE {dex_card} SET weight = -1 WHERE cid = %d', $card->cid);
}

/*
 * Returns a geocoded version of $card, if possible. If geocoding is not possible, return the original $card.
 */
function dex_geocode($card) {
  if (!is_null(variable_get('google_maps_api_key', NULL))) {
    $url = 'http://maps.google.com/maps/geo?q='
           . urlencode($card->street . ', '
           . $card->city . ' ' . $card->postal_code . ', '
           . $card->country)
           . '&output=csv&key=' . variable_get('google_maps_api_key', '');
    
    $response = drupal_http_request($url);
    
    $geocoding = explode(',', $response->data);
    
    if ($geocoding[2] != 0 || $geocoding[3] != 0) {
      $card->latitude = $geocoding[2];
      $card->longitude = $geocoding[3];
    }
  }
  
  return $card;
}

/*
 * Outputs geocoding within the HTML <head>.
 */
function dex_html_header($id, $type) {
  $countries = dex_get_iso3166_list();

  $cards = array();
  $res = db_query_range('SELECT * FROM {dex_card} WHERE ' . substr($type, 0, 1) . 'id = %d ORDER BY weight', $id, 0, 1);
  if ($row = db_fetch_object($res)) {
    $headers = array();
    if (isset($row->latitude) && isset($row->longitude)) {
      $headers[] = '<meta name="ICBM" content="' . $row->latitude . ', ' . $row->longitude . '">';
      $headers[] = '<meta name="geo.position" content="' . $row->latitude . ';' . $row->longitude . '">';
    }
    if (isset($row->country)) {
      if (isset($row->city) && isset($row->province)) {
        $headers[] = '<meta name="geo.placename" content="' . $row->city . ', ' . $row->province . ', ' . $countries[$row->country] . '">';
      }
      $headers[] = '<meta name="geo.region" content="' . $row->country . '">';
    }
    drupal_set_html_head(implode("\n", $headers));
  }
}

/*
 * Determines whether a tab should show for the given $node.
 */
function _dex_show_for_content_type($node) {
  // Skip out early if the user doesn't have access to edit Dex info
  if (!user_access('edit dex information')) {
    return FALSE;
  }

  $node_types = variable_get('dex_content_types', array());
  
  $all_zero = TRUE;
  foreach ($node_types as $type => $enabled) {
    if ($enabled) {
      $all_zero == FALSE;
    }
  }
  
  // Default to showing on all content types if none are checked.
  return ($all_zero || $node_types[$node->type]);
}

/*
 * Implementation of hook_menu().
 */
function dex_menu() {
  $items = array();

  $items['admin/settings/dex'] = array(
    'title' => 'Dex settings',
    'description' => 'Configuration for Dex contact management.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dex_settings_form'),
    'access arguments' => array('administer dex'),
  );

  $items['node/%node/dex'] = array(
    'title' => t('Contact cards'),
    'page callback' => 'dex_card_list_page',
    'page arguments' => array(1),
    'access arguments' => array(1),
    'access callback' => '_dex_show_for_content_type',
    'type' => MENU_LOCAL_TASK,
  );

  $items['node/%node/dex/list'] = array(
    'title' => t('List'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['node/%node/dex/add'] = array(
    'title' => t('Add'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
    'access arguments' => array(1),
    'access callback' => '_dex_show_for_content_type',
    'page callback' => 'dex_card_add_page',
    'page arguments' => array(1),
  );

  $items['node/%node/dex/map'] = array(
    'title' => t('Map'),
    'type' => MENU_LOCAL_TASK,
    'access' => user_access('edit dex information'),
    'access arguments' => array(1),
    'access callback' => '_dex_show_for_content_type',
    'page callback' => 'dex_card_map_page',
    'page arguments' => array(1),
  );
  
  $items['node/%node/dex/%dex_card/edit'] = array(
    'access arguments' => array(1),
    'access callback' => '_dex_show_for_content_type',
    'page callback' => 'dex_card_edit_page',
    'page arguments' => array(3),
    'type' => MENU_CALLBACK,
  );
  $items['node/%node/dex/%dex_card/delete'] = array(
    'access arguments' => array(1),
    'access callback' => '_dex_show_for_content_type',
    'page callback' => 'dex_card_delete_page',
    'page arguments' => array(3),
    'type' => MENU_CALLBACK,
  );        
  $items['node/%node/dex/%dex_card/primary'] = array(
    'access arguments' => array(1),
    'access callback' => '_dex_show_for_content_type',
    'page callback' => 'dex_card_primary_page',
    'page arguments' => array(3),
    'type' => MENU_CALLBACK,
  );

  // For users
  $items['user/%user/dex'] = array(
    'title' => t('Contact cards'),
    'page callback' => 'dex_card_list_page',
    'page arguments' => array(1),
    'access arguments' => array('administer dex'),
    'type' => MENU_LOCAL_TASK,
  );

  $items['user/%user/dex/list'] = array(
    'title' => t('List'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['user/%user/dex/add'] = array(
    'title' => t('Add'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
    'access arguments' => array('administer dex'),
    'page callback' => 'dex_card_add_page',
    'page arguments' => array(1),
  );

  $items['user/%user/dex/map'] = array(
    'title' => t('Map'),
    'type' => MENU_LOCAL_TASK,
    'access' => user_access('edit dex information'),
    'access arguments' => array('administer dex'),
    'page callback' => 'dex_card_map_page',
    'page arguments' => array(1),
  );
  
  $items['user/%user/dex/%dex_card/edit'] = array(
    'access arguments' => array('administer dex'),
    'page callback' => 'dex_card_edit_page',
    'page arguments' => array(3),
    'type' => MENU_CALLBACK,
  );
  $items['user/%user/dex/%dex_card/delete'] = array(
    'access arguments' => array('administer dex'),
    'page callback' => 'dex_card_delete_page',
    'page arguments' => array(3),
    'type' => MENU_CALLBACK,
  );        
  $items['user/%user/dex/%dex_card/primary'] = array(
    'access arguments' => array('administer dex'),
    'page callback' => 'dex_card_primary_page',
    'page arguments' => array(3),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function dex_card_map_page($object) {
  $type = NULL;
  if ($object->nid) {
    $type = 'node';
    $id = $object->nid;
  }
  else if ($object->uid) {
    $type = 'user';
    $id = $object->uid;
  }
  
  return dex_map($id, $type);
}

function dex_theme($existing, $type, $theme, $path) {
  return array(
    'dex_map_box' => array(
      'arguments' => array(
        'card' => NULL,
      ),
    ),
  );
}

function theme_dex_map_box($card) {
  $label = '';
  $after = '';
  $marker_text = array();

  $marker_text[] = $card->street;
  if ($card->additional != '') {
    $marker_text[] = $card->additional;
  }
  $marker_text[] = $card->city . ', ' . $card->province . ' ' . $card->postal_code;
  if ($card->phone != '') {
    $marker_text[] = '<a href="callto:' . $card->phone . '">' . $card->phone . '</a>';
    $after .= '<br />';
  }
  if ($card->url != '') {
    $marker_text[] = l($card->url, $card->url);
    $after .= '<br />';
  }
  if ($card->email != '') {
    $marker_text[] = l($card->email, 'mailto:' . $card->email);
    $after .= '<br />';
  }
  $label .= '<strong>' . $card->title . '</strong><br />';
  $label .= str_replace('"', '\"', implode('<br />', $marker_text)) . $after;
  return $label;
}

function dex_map($id, $type, $width = 500, $height = 500) {
  if (variable_get('google_maps_api_key', '') == '') { 
    return t('Please add a Google Maps API key to view maps.');
  }

  static $maps = 0;
  
  if ($maps == 0) {
     drupal_set_html_head('<script type="text/javascript" src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=' . variable_get('google_maps_api_key', '') . '"></script>'); 
  }
  
  $res = db_query('SELECT * FROM {dex_card} WHERE ' . substr($type, 0, 1) . 'id = %d', $id);

  // Load cards and calculate average longitude and latitude
  $cards = array();
  $longitude_sum = 0;
  $latitude_sum = 0;

  while($card = db_fetch_object($res)) {
    if ($card->longitude != 0 || $card->latitude != 0) {
      $longitude_sum += $card->longitude;
      $latitude_sum += $card->latitude;
      $cards[] = $card;
    }
  }
    
  $map = '';
  
  if (empty($cards)) { 
    return t('Please add at least one card (with geocoded information) to view a map.');
  }

  $average_longitude = $longitude_sum / count($cards);
  $average_latitude = $latitude_sum / count($cards);

  if ($maps == 0) {
    $js = 'function dex_create_marker(point, label) {
            var marker = new GMarker(point);
            GEvent.addListener(marker, "click", function() {
              marker.openInfoWindowHtml(label);
            });
            return marker;
          }'; 
    drupal_add_js($js, 'inline');
  }
  
  $map_id = 'dex_map_' . $maps;
  
  $js =
    'if (Drupal.jsEnabled) {
      $(document).ready(
        function () {
          if (GBrowserIsCompatible()) {
            var bounds = new GLatLngBounds();
            var map = new GMap2(document.getElementById("' . $map_id . '"));
            map.setCenter(new GLatLng(' . $average_latitude . ', ' . $average_longitude . '), 15);
            map.addControl(new GSmallMapControl());' . "\n";
  
  foreach($cards as $card) {
    $label = theme('dex_map_box', $card);
    //$js .= 'map.addOverlay(new GMarker(new GLatLng(' . $address->latitude . ', ' . $address->longitude . ')));' . "\n";
    $js .= 'bounds.extend(new GLatLng(' . $card->latitude . ', ' . $card->longitude . '));';
    $js .= 'map.addOverlay(dex_create_marker(new GLatLng(' . $card->latitude . ', ' . $card->longitude . '), "' . $label . '"));' . "\n";
  }          
  
  
  $js .= 'map.setZoom(map.getBoundsZoomLevel(bounds));
          map.setCenter(bounds.getCenter());';
  $js .= 
    '     }
        }
      )
      $(document).unload(
        function () {
          GUnload();
        }
      )
    }';
  drupal_add_js($js, 'inline');
  
  $map = '<div id="' . $map_id . '" style="width: ' . $width . 'px; height: ' . $height . 'px"></div>';

  $maps++;
  return $map;
}

function _dex_card_type($card) {
  if ($card->nid) {
    return 'node';
  }
  else if ($card->uid) {
    return 'user';
  }
}

function _dex_card_id($card) {
  if ($card->nid) {
    return $card->nid;
  }
  else if ($card->uid) {
    return $card->uid;
  }
}

function dex_card_primary_page($card) {
  dex_card_primary($card);
  drupal_set_message('Primary card set.');
  
  if ($card->nid) {
    drupal_goto('node/' . $card->nid . '/dex');
  }
  else if ($card->uid) {
    drupal_goto('user/' . $card->uid . '/dex');
  }
}

function dex_card_list_page($object) {
  $type = NULL;
  if ($object->nid) {
    $type = 'node';
    $id = $object->nid;
  }
  else if ($object->uid) {
    $type = 'user';
    $id = $object->uid;
  }
  
  if (!$type) {
    drupal_set_message('Invalid object type for Dex.', 'error');
    
    return ' ';
  }

  _dex_title($id, $type);

  $content = array();

  $cols = array(
    t('Title'),
    t('Operations'),
  );
  
  $rows = array();
  
  $res = db_query('SELECT * FROM {dex_card} WHERE ' . substr($type, 0, 1) . 'id = %d ORDER BY title', $id, $type);
  while ($row = db_fetch_object($res)) {
    $primary = '';
    if ($row->weight == -1) {
      $row->title .= t(' (Primary)');
    }
    else {
      $primary = ' | ' . l('make primary', $type . '/' . $id . '/dex/' . $row->cid . '/primary');
    }
  
    $marker_text = array();
    $marker_text[] = '<strong>' . $row->title . '</strong>';
    
    if ($row->street) {
      $marker_text[] = $row->street;
    }
    
    if ($row->additional != '') {
      $marker_text[] = $row->additional;
    }
    
    if ($row->city && $row->province) {
      $marker_text[] = trim($row->city . ', ' . $row->province . ' ' . $row->postal_code);
    }
    
    if ($row->phone != '') {
      $marker_text[] = '<a href="callto:' . $row->phone . '">' . $row->phone . '</a>';
    }
    if ($row->url != '') {
      $marker_text[] = l($row->url, $row->url);
    }
    if ($row->email != '') {
      $marker_text[] = l($row->email, 'mailto:' . $row->email);
    }
  
    $rows[] = array(
      implode('<br />', $marker_text),
      l('edit', $type . '/' . $id . '/dex/' . $row->cid . '/edit') . ' | ' .
      l('delete', $type . '/' . $id . '/dex/' . $row->cid . '/delete') . $primary,
    );
  }

  if (empty($rows)) {
    $content[] = '<p>' . t('No cards currently created for this node.') . '</p>';
  }
  else {
    $content[] = theme('table', $cols, $rows);
  }

  return implode("\n", $content);
}

function dex_card_delete_page($card) {
  _dex_title($card->id, $card->type);
  return drupal_get_form('dex_card_delete_form', $card);
}

function dex_card_delete_form(&$form_state, $card) {
  $form = array();
  
  $form['cid'] = array(
    '#type' => 'hidden',
    '#value' => $card->cid,
  );
  
  $form[] = array(
    '#value' => '<p>' . t('Delete card %card?', array('%card' => $card->title)) . '</p>',
  );

  $form[] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  $form[] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  
  return $form;
}

function dex_card_delete_form_submit($form, &$form_state) {
  $deleting = dex_card_load($form_state['values']['cid']);
  if ($form_state['values']['op'] == t('Delete')) {
    db_query('DELETE FROM {dex_card} WHERE cid = %d', $deleting->cid);
    
    // Promote another card to primary status if the deleted card was primary
    if ($deleting->weight == -1) {
      if ($deleting->nid) {
        $cid = db_result(db_query('SELECT cid FROM {dex_card} WHERE nid = %d', $deleting->nid));
      }
      else if ($deleting->uid) {
        $cid = db_result(db_query('SELECT cid FROM {dex_card} WHERE uid = %d', $deleting->uid));
      }
      
      if ($cid) {
        dex_card_primary(dex_card_load($cid));
      }
    }
    
    drupal_set_message(t('Card deleted.'));
  }
  
  if ($deleting->nid) {
    $form_state['redirect'] = 'node/' . $deleting->nid . '/dex';
  }
  else if ($deleting->uid) {
    $form_state['redirect'] = 'user/' . $deleting->uid . '/dex';
  }
}

/*
 * Add geocoding information for nodes with cards. Delete cards when the corresponding node is deleted.
 */
function dex_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if ($op == 'view' && !$a3) {
    $node->content['geocode'] = array(
      '#value' => dex_geocode_xml($node->nid, 'node'),
      '#weight' => 10,
    );
    dex_html_header($node->nid, 'node');
  }
  else if ($op == 'delete') {
    db_query('DELETE FROM {dex_card} WHERE nid = %d', $node->nid);
  }
}

/*
 * Add geocoding information for users with cards. Delete cards when the corresponding user is deleted.
 */
function dex_user($op, &$edit, &$account, $category = NULL) {
  if ($op == 'view') {
    $account->content['geocode'] = array(
      '#value' => dex_geocode_xml($account->uid, 'user'),
      '#weight' => 10,
    );
    dex_html_header($account->uid, 'user');
  }
  else if ($op == 'delete') {
    db_query('DELETE FROM {dex_card} WHERE uid = %d', $account->nid);
  }
}

/*
 * Build the geocoding microformat XML for a given $type and $id.
 */
function dex_geocode_xml($id, $type) {
  $countries = dex_get_iso3166_list();
  $res = db_query('SELECT * FROM {dex_card} WHERE ' . substr($type, 0, 1) . 'id = %d ORDER BY title', $id);
  
  $has_content = FALSE;
  
  $content = array();
  $content[] = '<span style="display: none;">';

  while ($row = db_fetch_object($res)) {
      $content[] = '<span class="vcard">';
      
      if ($row->longitude && $row->latitude) {
        $content[] = '<span class="geo-dec geo">';
        $content[] = '<span class="latitude">' . $row->latitude . '</span>;';
        $content[] = '<span class="longitude">' . $row->longitude . '</span>';
        $content[] = '</span>﻿';
      }
      
      if ($row->title) {
        $content[] = '<span class="fn">' . $row->title . '</span>';
      }

      $content[] = '<span class="adr">';

      if ($row->street && user_access('view user street addresses')) {
        $content[] = '<span class="street-address">' . $row->street . '</span>';
      };
      
      if ($row->city) {
        $content[] = '<span class="locality">' . $row->city . '</span>';
      }
      
      if ($row->province) {
        $content[] = '<span class="region" title="' . $row->province . '">' . $row->province . '</span>';
      }
      
      if ($row->postal_code) {
        $content[] = '<span class="postal-code">' . $row->postal_code . '</span>';
      }
      
      if ($row->country) {
        $content[] = '<span class="country-name">' . $countries[$row->country] . '</span>';
      }
      
      $content[] = '</span>';
      
      if ($row->phone) {
        $content[] = '<span class="tel">' . $row->phone . '</span>';
      }
      
      if ($row->url) {
        $content[] = '<span class="url">' . $row->url . '</span>';
      }
      
      if ($row->email) {
        $content[] = '<span class="email">' . $row->email . '</span>';
      }
          
      $content[] = '</span>';
      $content[] = '';
      
      $has_content = TRUE;
  }
  
  $content[] = '</span>';
  
  if ($has_content) {
    $output = implode("\n", $content);
  } 
  else {
    $output = '';
  }
  
  return $output;
}

function dex_card_add_page($object) {
  $card = new stdClass();
  
  if ($object->nid) {
    $card->nid = $object->nid;
  }
  else if ($object->uid) {
    $card->uid = $object->uid;
  }

  return drupal_get_form('dex_card_edit_form', $card);
}

function dex_card_edit_page($card) {
  _dex_title($card->type, $card->id);
  return drupal_get_form('dex_card_edit_form', $card);
}

function dex_card_edit_form(&$form_state, $card) {
  $form = array();
  
  if ($card->cid) {
    $form['cid'] = array(
      '#type' => 'hidden',
      '#value' => $card->cid,
    );
    $form['weight'] = array(
      '#type' => 'hidden',
      '#value' => $card->weight,
    );
  }
  else {
    // Set the title of the page for new cards
    if ($card->nid) {
      $node = node_load($card->nid);   
      $default_title = $node->title;
    }
    else if ($card->uid) {
      $user = user_load($card->uid);
      $default_title = $user->name;
    }
    $form['weight'] = array(
      '#type' => 'hidden',
      '#value' => 0,
    );
  }
  
  if ($card->nid) {
    $form['nid'] = array(
      '#type' => 'hidden',
      '#value' => $card->nid,
    );
  }
  else if ($card->uid) {
    $form['uid'] = array(
      '#type' => 'hidden',
      '#value' => $card->uid,
    );
  }
  
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Location/address title'),
    '#required' => TRUE,
    '#default_value' => $card->title ? $card->title : $default_title,
    '#description' => t('Use location titles to distinguish multiple locations from each other. For example, a story about pizzerias might have locations titled "East Village Pizza" and "The Flatiron Grill."'),
  );
  
  $form['street'] = array(
    '#type' => 'textfield',
    '#title' => t('Street'),
    '#default_value' => $card->street,
  );

  $form['additional'] = array(
    '#type' => 'textfield',
    '#default_value' => $card->additional,
    '#description' => t('The second line of the street address is ignored for geocoding.'),
  );

  $form['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#default_value' => $card->city,
  );

  $form['province'] = array(
    '#type' => 'textfield',
    '#title' => t('State/province'),
    '#default_value' => $card->province,
  );

  $form['postal_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Postal code'),
    '#default_value' => $card->postal_code,
  );

  $form['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone number'),
    '#default_value' => $card->phone,
    '#description' => t('Include the extension, if necessary'),
  );
  
  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Website URL'),
    '#default_value' => $card->url,
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#default_value' => $card->email,
  );

  if (!isset($address->country)) {
    $address->country = variable_get('dex_default_country', 'us');
  }

  $form['country'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => dex_get_iso3166_list(),
    '#required' => TRUE,
    '#default_value' => $address->country,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $card->cid ? t('Update') : t('Add'),
  );
  
  return $form;
}

function dex_card_edit_form_submit($form, &$form_state) {
  $card = new stdClass();
  $card->weight = $form_state['values']['weight'];
  $card->title = $form_state['values']['title'];
  $card->street = $form_state['values']['street'];
  $card->additional = $form_state['values']['additional'];
  $card->city = $form_state['values']['city'];
  $card->province = $form_state['values']['province'];
  $card->postal_code = $form_state['values']['postal_code'];
  $card->country = $form_state['values']['country'];
  $card->latitude = $form_state['values']['latitude'];
  $card->longitude = $form_state['values']['longitude'];
  $card->phone = $form_state['values']['phone'];
  $card->url = $form_state['values']['url'];
  $card->email = $form_state['values']['email'];

  // Also add the cid if we're editing an existing card.
  if (array_key_exists('cid', $form_state['values'])) {
    $card->cid = $form_state['values']['cid'];
  }

  if (array_key_exists('nid', $form_state['values'])) {
    $card->nid = $form_state['values']['nid'];
    
    // Set the card as primary if there is no existing primary card.
    $count = db_result(db_query_range('SELECT COUNT(*) FROM {dex_card} WHERE nid = %d', $card->nid, 0, 1));
    if (!$count) {
      $card->weight = -1;
    }
  }
  else if (array_key_exists('uid', $form_state['values'])) {
    $card->uid = $form_state['values']['uid'];

    // Set the card as primary if there is no existing primary card.
    $count = db_result(db_query_range('SELECT COUNT(*) FROM {dex_card} WHERE uid = %d', $card->uid, 0, 1));
    if (!$count) {
      $card->weight = -1;
    }
  }

  // Attempt to geocode the card.
  $card = dex_geocode($card);

  if (!$card->longitude && !$card->latitude) {
    drupal_set_message(t('Geocoding the card was unsuccessful. Check your Google Maps API key.'), 'warning');
  }

  $success = dex_card_save($card);

  // Provide appropriate feedback.
  if ($success) {
    if ($success === TRUE) {
      drupal_set_message(t('Card updated.'));
    }
    else {
      drupal_set_message(t('Card added.'));
    }
  }
  else {
    drupal_set_message(t('Card operation failed.'), 'error');
  }
  
  if ($card->nid) {
    $form_state['redirect'] = 'node/' . $card->nid . '/dex';
  }
  else if ($card->uid) {
    $form_state['redirect'] = 'user/' . $card->uid . '/dex';
  }
}

function _dex_title($id, $type) {
  if ($type == 'node') {
    $node = node_load($id);
    drupal_set_title($node->title);
  }
  else if ($type == 'user') {
    $user = user_load(array('uid' => $id));
    drupal_set_title($user->name);
  }
}

function dex_perm() {
  return array('edit dex information', 'administer dex', 'view user street addresses');
}

function dex_get_iso3166_list() {
  return array(
    "ad" => "Andorra",
    "af" => "Afghanistan",
    "ax" => "Aland Islands",
    "al" => "Albania",
    "dz" => "Algeria",
    "as" => "American Samoa",
    "ao" => "Angola",
    "ai" => "Anguilla",
    "aq" => "Antarctica",
    "ag" => "Antigua and Barbuda",
    "ar" => "Argentina",
    "am" => "Armenia",
    "aw" => "Aruba",
    "au" => "Australia",
    "at" => "Austria",
    "az" => "Azerbaijan",
    "bs" => "Bahamas",
    "bh" => "Bahrain",
    "bd" => "Bangladesh",
    "bb" => "Barbados",
    "by" => "Belarus",
    "be" => "Belgium",
    "bz" => "Belize",
    "bj" => "Benin",
    "bm" => "Bermuda",
    "bt" => "Bhutan",
    "bo" => "Bolivia",
    "ba" => "Bosnia and Herzegovina",
    "bw" => "Botswana",
    "bv" => "Bouvet Island",
    "br" => "Brazil",
    "io" => "British Indian Ocean Territory",
    "vg" => "British Virgin Islands",
    "bn" => "Brunei",
    "bg" => "Bulgaria",
    "bf" => "Burkina Faso",
    "bi" => "Burundi",
    "kh" => "Cambodia",
    "cm" => "Cameroon",
    "ca" => "Canada",
    "cv" => "Cape Verde",
    "ky" => "Cayman Islands",
    "cf" => "Central African Republic",
    "td" => "Chad",
    "cl" => "Chile",
    "cn" => "China",
    "cx" => "Christmas Island",
    "cc" => "Cocos (Keeling) Islands",
    "co" => "Colombia",
    "km" => "Comoros",
    "cg" => "Congo (Brazzaville)",
    "cd" => "Congo (Kinshasa)",
    "ck" => "Cook Islands",
    "cr" => "Costa Rica",
    "hr" => "Croatia",
    "cu" => "Cuba",
    "cy" => "Cyprus",
    "cz" => "Czech Republic",
    "dk" => "Denmark",
    "dj" => "Djibouti",
    "dm" => "Dominica",
    "do" => "Dominican Republic",
    "tl" => "East Timor",
    "ec" => "Ecuador",
    "eg" => "Egypt",
    "sv" => "El Salvador",
    "gq" => "Equatorial Guinea",
    "er" => "Eritrea",
    "ee" => "Estonia",
    "et" => "Ethiopia",
    "fk" => "Falkland Islands",
    "fo" => "Faroe Islands",
    "fj" => "Fiji",
    "fi" => "Finland",
    "fr" => "France",
    "gf" => "French Guiana",
    "pf" => "French Polynesia",
    "tf" => "French Southern Territories",
    "ga" => "Gabon",
    "gm" => "Gambia",
    "ge" => "Georgia",
    "de" => "Germany",
    "gh" => "Ghana",
    "gi" => "Gibraltar",
    "gr" => "Greece",
    "gl" => "Greenland",
    "gd" => "Grenada",
    "gp" => "Guadeloupe",
    "gu" => "Guam",
    "gt" => "Guatemala",
    "gg" => "Guernsey",
    "gn" => "Guinea",
    "gw" => "Guinea-Bissau",
    "gy" => "Guyana",
    "ht" => "Haiti",
    "hm" => "Heard Island and McDonald Islands",
    "hn" => "Honduras",
    "hk" => "Hong Kong S.A.R., China",
    "hu" => "Hungary",
    "is" => "Iceland",
    "in" => "India",
    "id" => "Indonesia",
    "ir" => "Iran",
    "iq" => "Iraq",
    "ie" => "Ireland",
    "im" => "Isle of Man",
    "il" => "Israel",
    "it" => "Italy",
    "ci" => "Ivory Coast",
    "jm" => "Jamaica",
    "jp" => "Japan",
    "je" => "Jersey",
    "jo" => "Jordan",
    "kz" => "Kazakhstan",
    "ke" => "Kenya",
    "ki" => "Kiribati",
    "kw" => "Kuwait",
    "kg" => "Kyrgyzstan",
    "la" => "Laos",
    "lv" => "Latvia",
    "lb" => "Lebanon",
    "ls" => "Lesotho",
    "lr" => "Liberia",
    "ly" => "Libya",
    "li" => "Liechtenstein",
    "lt" => "Lithuania",
    "lu" => "Luxembourg",
    "mo" => "Macao S.A.R., China",
    "mk" => "Macedonia",
    "mg" => "Madagascar",
    "mw" => "Malawi",
    "my" => "Malaysia",
    "mv" => "Maldives",
    "ml" => "Mali",
    "mt" => "Malta",
    "mh" => "Marshall Islands",
    "mq" => "Martinique",
    "mr" => "Mauritania",
    "mu" => "Mauritius",
    "yt" => "Mayotte",
    "mx" => "Mexico",
    "fm" => "Micronesia",
    "md" => "Moldova",
    "mc" => "Monaco",
    "mn" => "Mongolia",
    "me" => "Montenegro",
    "ms" => "Montserrat",
    "ma" => "Morocco",
    "mz" => "Mozambique",
    "mm" => "Myanmar",
    "na" => "Namibia",
    "nr" => "Nauru",
    "np" => "Nepal",
    "nl" => "Netherlands",
    "an" => "Netherlands Antilles",
    "nc" => "New Caledonia",
    "nz" => "New Zealand",
    "ni" => "Nicaragua",
    "ne" => "Niger",
    "ng" => "Nigeria",
    "nu" => "Niue",
    "nf" => "Norfolk Island",
    "kp" => "North Korea",
    "mp" => "Northern Mariana Islands",
    "no" => "Norway",
    "om" => "Oman",
    "pk" => "Pakistan",
    "pw" => "Palau",
    "ps" => "Palestinian Territory",
    "pa" => "Panama",
    "pg" => "Papua New Guinea",
    "py" => "Paraguay",
    "pe" => "Peru",
    "ph" => "Philippines",
    "pn" => "Pitcairn",
    "pl" => "Poland",
    "pt" => "Portugal",
    "pr" => "Puerto Rico",
    "qa" => "Qatar",
    "re" => "Reunion",
    "ro" => "Romania",
    "ru" => "Russia",
    "rw" => "Rwanda",
    "sh" => "Saint Helena",
    "kn" => "Saint Kitts and Nevis",
    "lc" => "Saint Lucia",
    "pm" => "Saint Pierre and Miquelon",
    "vc" => "Saint Vincent and the Grenadines",
    "ws" => "Samoa",
    "sm" => "San Marino",
    "st" => "Sao Tome and Principe",
    "sa" => "Saudi Arabia",
    "sn" => "Senegal",
    "rs" => "Serbia",
    "cs" => "Serbia And Montenegro",
    "sc" => "Seychelles",
    "sl" => "Sierra Leone",
    "sg" => "Singapore",
    "sk" => "Slovakia",
    "si" => "Slovenia",
    "sb" => "Solomon Islands",
    "so" => "Somalia",
    "za" => "South Africa",
    "gs" => "South Georgia and the South Sandwich Islands",
    "kr" => "South Korea",
    "es" => "Spain",
    "lk" => "Sri Lanka",
    "sd" => "Sudan",
    "sr" => "Suriname",
    "sj" => "Svalbard and Jan Mayen",
    "sz" => "Swaziland",
    "se" => "Sweden",
    "ch" => "Switzerland",
    "sy" => "Syria",
    "tw" => "Taiwan",
    "tj" => "Tajikistan",
    "tz" => "Tanzania",
    "th" => "Thailand",
    "tg" => "Togo",
    "tk" => "Tokelau",
    "to" => "Tonga",
    "tt" => "Trinidad and Tobago",
    "tn" => "Tunisia",
    "tr" => "Turkey",
    "tm" => "Turkmenistan",
    "tc" => "Turks and Caicos Islands",
    "tv" => "Tuvalu",
    "vi" => "U.S. Virgin Islands",
    "ug" => "Uganda",
    "ua" => "Ukraine",
    "ae" => "United Arab Emirates",
    "gb" => "United Kingdom",
    "us" => "United States",
    "um" => "United States Minor Outlying Islands",
    "uy" => "Uruguay",
    "uz" => "Uzbekistan",
    "vu" => "Vanuatu",
    "va" => "Vatican",
    "ve" => "Venezuela",
    "vn" => "Vietnam",
    "wf" => "Wallis and Futuna",
    "eh" => "Western Sahara",
    "ye" => "Yemen",
    "zm" => "Zambia",
    "zw" => "Zimbabwe",
  );
}

function dex_settings_form() {
  $form = array();
  
  $form['api_key'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Maps API key'),
  );

  $form['api_key'][] = array(
    '#value' => '<strong>1. ' . l('Get the key here.', 'http://www.google.com/maps/api_signup?url=' . url(NULL, array('absolute' => TRUE)), array('target' => 'blank')) . '</strong>',
  );
  
  $form['api_key']['google_maps_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('2. Paste the key here'),
    '#default_value' => variable_get('google_maps_api_key', ''),
  );
  
  $form['other'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other options'),
  );
  
  $form['other']['dex_nearby_same_content_type_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only compute nearby locations for the same content type'),
    '#default_value' => variable_get('dex_nearby_same_content_type_only', FALSE),
  );

  $form['other']['dex_nearby_distance'] = array(
    '#type' => 'textfield',
    '#title' => t('Distance (in km) to nearby locations'),
    '#default_value' => variable_get('dex_nearby_distance', 10),
  );
  
  $form['other']['dex_nearby_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of nearby locations to return'),
    '#default_value' => variable_get('dex_nearby_limit', 10),
  );
    
  $form['other']['dex_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types to show "Contact cards" tab for'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('dex_content_types', array()),
  );
  
  return system_settings_form($form);
}
